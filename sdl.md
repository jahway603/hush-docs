# Silent Dragon Lite

This documentation is how you setup Silent Dragon Lite (SDL), which is the Hush lite wallet featuring HushChat.

## Download and install SDL

**Make sure you make a paper backup of your seed phrases!**

- For Binary releases, [the download link is here](https://github.com/MyHush/SilentDragonLite/releases)
- For source code so you can compile your own, [the link is here](https://github.com/MyHush/SilentDragonLite)

### See also
- [How-to create your own SDL server](https://gitlab.com/jahway603/hush-lite-server)
