# Hush Docs

<p align="left">
    <a href="https://twitter.com/MyHushTeam">
        <img src="https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Ftwitter.com%2Fmyhushteam"
            alt="MyHushTeam's Twitter"></a>
    <a href="https://twitter.com/intent/follow?screen_name=MyHushTeam">
        <img src="https://img.shields.io/twitter/follow/MyHushTeam?style=social&logo=twitter"
            alt="follow on Twitter"></a>
    <a href="https://fosstodon.org/@myhushteam">
        <img src="https://img.shields.io/badge/Mastodon-MyHushTeam-blue"
            alt="follow on Mastodon"></a>
    <a href="https://www.reddit.com/r/Myhush/">
        <img src="https://img.shields.io/reddit/subreddit-subscribers/Myhush?style=social"
            alt="MyHushTeam's Reddit"></a>
    <a href="https://t.me/Hush_Coin">
        <img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white" alt="MyHushTeam's Telegram"></a>
</p>

Documentation from the Hush Community

## Community Documentation

### Wallets

If you are looking to setup a Hush wallet, then you have the following two choices dependant upon your requirements:

- [Setup Hush full node with hushd](sd.md)
- [Setup Hush lite wallet featuring Hushchat](sdl.md)

As for mobile wallets:
- Hush Android app is a companion app which pairs with either of the above two options.
- Hush does not have an iOS app yet, care to make one?

#### Servers?

If you know what you're doing, and want to setup a server to help the Hush community, then the following documents are for you:

- [Setup your own Hush Lite Wallet Server](https://gitlab.com/jahway603/hush-lite-server)
- [Setup your own Hush Wormhole Server](wormhole.md) (**Work in progress**)

## Support Links

- [Hush general Telegram chat](https://t.me/Hush_Coin)
- [Hush tech support via Telegram](https://t.me/hush8support)

## How to follow Hush

- [Twitter](https://twitter.com/MyHushTeam)
- [Telegram](https://t.me/Hush_Coin)

## Exchanges

- [Safe Trade](https://safe.trade/) - also believe you need 2FA enabled to be a Hushter on their exchange

##### Created 2020