# Silent Dragon

This documentation is how you setup Silent Dragon, which is the Hush full node wallet.

## 1) Setup hushd

- If you have Linux, [click here](hushd-desktop-linux.md)
- If you have Windows, [click here](hushd-desktop-windows.md)
- If you have Mac, <_please contribute_>

## 2) Install Silent Dragon

**Make sure you make a paper backup of your seed phrases!**

Pick either the [binary release](https://github.com/MyHush/SilentDragon/releases) or [compile it yourself](https://github.com/MyHush/SilentDragon/blob/master/README.md). I personally compile but some prefer binaries.
